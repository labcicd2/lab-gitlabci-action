FROM openjdk:17-jdk-slim

LABEL maintainer="Boubacar Siddy DIALLO boubasiddy00@gmail.com"

EXPOSE 8080

ADD target/lab-gitlabci-action.jar lab-gitlabci-action.jar

ENTRYPOINT ["java", "-jar", "lab-gitlabci-action.jar"]